<?php
/**
 * Ancora News 
 */

/**
 * Class Ancora_News_Resource_Category
 * @author Valeria Ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */

class Ancora_News_Model_Resource_Category extends Mage_Core_Model_Resource_Db_Abstract 
{
 /**
  * _construct
  */   
 
    public function _construct()
    {
        $this->_init('ancora_news/category' ,'category_id');
    }
}