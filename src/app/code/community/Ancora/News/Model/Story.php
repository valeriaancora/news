<?php

/**
 * Ancora_News_Model_Story
 *
 * Ancora News Story Model
 *
 * @method string getCode()
 * @method setCode(string $code)
 * @method string getTitle()
 * @method setTitle(string $title)
 * @method boolean getStatus()
 * @method setStatus(boolean $status)
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method setUpdatedAt(string $updatedAt)
 * @author Valeria Ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */

class Ancora_News_Model_Story extends Mage_Core_Model_Abstract
{
    /**
     * $_eventPrefix
     * @var string
     */

    protected $_eventPrefix = 'ancora_news_story';


    /**
     * $_eventObject
     * @var string
     */

    protected $_eventObject = 'story' ;


    /**
     * _construct
     */

    public function _construct(){
        $this->_init('ancora_news/story');
    }


    /**
     * _beforeSave
     * @return $this
     */

    protected  function _beforeSave()
    {
        parent::_beforeSave(); //
        $this->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate());
        return $this;
    }

}