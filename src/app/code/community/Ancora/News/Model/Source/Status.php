<?php
/**
 * Ancora_News_Model_Source_Status
 *
 * Ancora News Model Source_Status
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @liceAncoranse GNU
 *
 */
class Ancora_News_Model_Source_Status
{
    public function toOptionArray()
    {
        return array(
          array(
              'value' => 0,
              'label' => Mage::helper('ancora_news')->__('Disabled'),
          ) ,
            array(
              'value' => 1,
              'label' => Mage::helper('ancora_news')->__('Enabled'),
          ),
        );
    }

    /**
     * @return array
     */

    public function toGridArray()
    {
      $array = array();
      foreach($this->toOptionArray() as $op){
          $array[$op['value']] = $op['label'];
      }
      return $array;
    }
}