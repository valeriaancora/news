<?php
/**
 * Ancora News
 */

/**
 * Class Ancora_News_Model_Categoryobserver
 * @author Valeria Ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package CMS
 * @license GNU version 3
 */
class Ancora_News_Model_Categoryobserver
{
    /**
     * CreateLogEntry
     *
     * Create a log entry when a category is saved
     * @param Varien_Event_Observer $observer
     * @return Varien_Event_Observer
     */

    public function createLogEntry(Varien_Event_Observer $observer)
    {
        //get category object
        $category = $observer->getData('category');

        //quality control
        if (!$category || !$category->getId()){
            return $observer;
        }

        //save the log
        Mage::log('Category saved. ID: ' . $category->getId());
        return $observer;
    }
}