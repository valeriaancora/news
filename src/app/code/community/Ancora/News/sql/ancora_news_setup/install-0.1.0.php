<?php
/**
 * Ancora News Setup
 */

/**
 * Ancora News SQL Installer.
 * @author Valeria Ancora <valeria.ancora@thinkopen.com>
 * @version 0.1.0
 * @package CMS
 * @license GNU version 3
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer -> startSetup(); //start setup

//prepare table for ancora_news/category
$tableCategory = $installer
                    ->getConnection()
                    ->newTable($installer->getTable('ancora_news/category')); //nome_model/entity

//populate the table
/**
 * paramentri in AddColumn
 *      nome Colonna
 *      tipo di colonna
 *      size
 *      array
 *          nullable = non può essere nullo
 *          identity = auto increment
 *          primary = chiave primaria
 *          default = per i timestamp c'è un valore di default per Magento
 */

$tableCategory->addColumn(
    'category_id',
    Varien_Db_Ddl_Table::TYPE_INTEGER,
    null,
    array ('nullable' => false, 'identity' => true, 'primary' => true),
    'Category Id'
)->addColumn(
    'code',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    64,
    array ('nullable' => false),
    'Category Code'
)->addColumn(
    'title',
    Varien_Db_Ddl_Table::TYPE_TEXT,
    256,
    array ('nullable' => false),
    'Category Title'
)->addColumn(
    'status',
    Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    null,
    array ('nullable' => false),
    'Category Status'
)->addColumn(
    'created_at',
    Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    null,
    array ('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT),
    'Category Created'
)->addColumn(
    'updated_at',
    Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
    null,
    array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE),
    'Category Updated'
)->setComment('News Category Table');

//create table
try{
    $installer->getConnection()->createTable($tableCategory);
} catch (Exception $e){
    Mage::logException($e);
}

$installer->endSetup(); //endSetup