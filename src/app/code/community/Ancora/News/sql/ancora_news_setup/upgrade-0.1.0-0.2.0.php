<?php
/**
 * Ancora Setup Story Table
 */

/**
 * Ancora News SQL Installer
 * @author Valeria Ancora valeria.ancora@thinkopen.it
 * @version 0.1.0
 * @package CMS
 * @license GNU version 3
 */
$installer = $this;
$installer->startSetup();

$tableStory = $installer
    ->getConnection()
    ->newTable($installer->getTable('ancora_news/story'));

    $tableStory->addColumn(
        'story_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array('nullable' => false, 'identity' => true ,'primary' => true),
        "Store Id"
    )->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        128,
        array('nullable' => false),
        'Story Title'
    )->addColumn(
        'thumbnail_path',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        128,
        array('nullable' => false),
        'Story Thumbnail Image'
    )->addColumn(
        'content',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array('nullable' => false),
        'Story Content'
    )->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        null,
        array('nullable' => false),
        'Story Status'
    )->addColumn(
        'category_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array('nullable' => false),
        'News Story Category ID'
    )->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT),
        'Story Created At'
    )->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE),
        'Story Updated At'
    )->addForeignKey(
        $installer->getFkName(
            'ancora_news/story',    //tabella corrente
            'category_id',    //colonna chiave esterna
            'ancora_news/category',    //tabella categoria in cui c'è chiave primaria collegata a chiave esterna
            'category_id'    //chiave primaria a cui legare chiave esterna
        ),
        'category_id',
        $installer->getTable('ancora_news/category'),
        'category_id'
    )->setComment("News Story Table");

try{
    $installer->getConnection()->createTable($tableStory);
}catch (Exception $e){
    Mage::logException($e);
}

$installer->endSetup();