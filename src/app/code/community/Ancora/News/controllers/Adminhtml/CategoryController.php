<?php
/**
 * Ancora_News_Adminhtml_CategoryController
 *
 * Ancora News Adminhtml CategoryController
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @liceAncoranse GNU
 *
 */
class Ancora_News_Adminhtml_CategoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * newAction
     */
  public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * editAction
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * saveAction
     */
    public function saveAction(){

        //get category id
        $categoryId = $this->getRequest()->getParam('category_id');

        if($categoryId){
            //load object
            $model = Mage::getModel('ancora_news/category')->load($categoryId);

            //verify if this is a valid object
            if (!$model || $model->getId()){
            Mage::getSingleton('adminhtml/session')->addError($this->__('There was an error when loading the category. Please, return to the previous page and try again'));
        return $this->_redirect('*/*/index');
        }
    } else {
        $model = Mage::getModel('ancora_news/category');
        }

        //manipulate object
        try{

            $model->setCode($this->getRequest()->getParam('code'));
            $model->setTitle($this->getRequest()->getParam('title'));
            $model->setStatus($this->getRequest()->getParam('status'));

            $model->save();

        } catch (Exception $e){

            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('There was an error when loading the category. Please, return to the previous page and try again'));
            return $this->_redirect('*/*/index');

        }

        Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The category was successfully saved'));


        if($this->getRequest()->getParam('back') && $this->getRequest()->getParam('back') == 'edit') {
            return $this->_redirect('*/*/edit', array('category_id' => $model->getId()));
        }

        return $this->_redirect('*/*/index');

    }

    /**
     * deleteAction
     */
    public function deleteAction(){

    }

    /**
     *
     * _isAllowed
     * @return Mage_Core_Model_Abstract
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ancora_news');
    }
}