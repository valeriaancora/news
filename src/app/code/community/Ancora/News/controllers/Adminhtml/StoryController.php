<?php
/**
 * Ancora_News_Adminhtml_StoryController
 *
 * Ancora News Adminhtml StoryController
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @liceAncoranse GNU
 *
 */
class Ancora_News_Adminhtml_StoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * editAction
     */
    public function editAction()
    {

    }

    /**
     * saveAction
     */
    public function saveAction(){

    }

    /**
     * deleteAction
     */
    public function deleteAction(){

    }

    /**
     *
     * _isAllowed
     * @return Mage_Core_Model_Abstract
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ancora_news');
    }
}