<?php
/**
 * Ancora_News_Block_Adminhtml_Renderer_Status
 *
 * Ancora_News_Block_Adminhtml_Renderer_Status
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @liceAncoranse GNU
 *
 */
class Ancora_News_Block_Adminhtml_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
       if ($row->getStatus() == 0){
           return '<span class="grid-severity-minor"><span>'. $this->__('Disabled') . '</span></span>';
       }else{
           return '<span class="grid-severity-notice"><span>'. $this->__('Enabled') . '</span></span>';

       }
    }
}