<?php
/**
 * Ancora_News__Block_Adminhtml_Category_Grid
 *
 * Ancora News Adminhtml Category Grid
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @license GNU
 *
 */

class Ancora_News_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('category_id');
        $this->setDefaultSort('category_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ancora_news/category')->getCollection();
        $this-> setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'category_id',
            array(
                'index' => 'category_id',
                'header' => $this->__('ID'),
                'width' => 50,
                'type' => 'number',
            )
        );
        $this->addColumn(
            'code',
            array(
                'index' => 'code',
                'header' => $this->__('Code'),
                'width' => 150,
                'type' => 'text',
            )
        );
        $this->addColumn(
        'title',
        array(
            'index' => 'title',
            'header' => $this->__('Title'),
            'type' => 'text',
        )
    );
        $statusValue = Mage::getModel('ancora_news/source_status')->toGridArray();
        $this->addColumn(
            'status',
            array(
                'index' => 'status',
                'header' => $this->__('Status'),
                'width' => 100,
                'type' => 'options',
                'options' => $statusValue,
                'renderer' => 'ancora_news/adminhtml_renderer_status'
            )
        );
        $this->addColumn('action',
            array(
                'header' =>  Mage::helper('customer')->__('Actions'),
                'width' => 100,
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => $this->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'category_id',
                    ),
                    array(
                        'caption' => $this->__('Delete'),
                        'url' => array('base' => '*/*/delete'),
                        'field' => 'category_id',
                        'confirm' => $this->__('Are you sure you want to do this?'),
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * getRowUrl
     *
     * @param $item
     * @return string
     */

    public function getRowUrl($item)
    {
        return $this->getUrl('*/*/edit', array('category_id' => $item->getId()));
    }
}