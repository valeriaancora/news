<?php
/**
 * Ancora_News__Block_Adminhtml_Edit_Tabs
 *
 * Ancora News Adminhtml Edit Tabs
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @license GNU
 *
 */
class Ancora_News_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Ancora_News_Block_Adminhtml_Category_Edit_Tabs constructor.
     */

    public function __construct()
    {
        parent::__construct();
        $this->setId('ancora_news_category_edit_tabs');
        $this->setDestElementId('ancora_news_category_edit_form');
    }

    /**
     * _beforeToHtml
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */

    public function _beforeToHtml()
    {
        $this->addTab(
            'category_details',
            array(
                'label' => Mage::helper('ancora_news')->__('Category Details'),
                'title' => Mage::helper('ancora_news')->__('Category Details'),

            )
        );
        return parent::_beforeToHtml();
    }
}