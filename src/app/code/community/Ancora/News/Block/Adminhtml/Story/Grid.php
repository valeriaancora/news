<?php
/**
 * Ancora_News__Block_Adminhtml_Story_Grid
 *
 * Ancora News Adminhtml Story Grid
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @license GNU
 *
 */
class Ancora_News_Block_Adminhtml_Story_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('story_id');
        $this->setDefaultSort('story_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection= Mage::getModel('ancora_news/story')->getCollection();
        $this-> setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'story_id',
            array(
                'index' => 'story_id',
                'header' => $this->__('ID'),
                'width' => 50,
                'type' => 'number',
            )
        );
        $this->addColumn(
            'title',
            array(
                'index' => 'title',
                'header' => $this->__('Title'),
                'type' => 'text',
            )
        );
        $this->addColumn(
            'created_at',
            array(
                'index' => 'created_at',
                'header' => $this->__('Created at'),
                'width' => 150,
                'type' => 'timestamp',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'index' => 'updated_at',
                'header' => $this->__('Updated at'),
                'width' => 150,
                'type' => 'timestamp',
            )
        );
        $this->addColumn(
            'category_id',
            array(
                'index' => 'category_id',
                'header' => $this->__('Category id'),
                'width' => 150,
                'type' => 'number',
            )
        );
        $statusValue = Mage::getModel('ancora_news/source_status')->toGridArray();
        $this->addColumn(
            'status',
            array(
                'index' => 'status',
                'header' => $this->__('Status'),
                'width' => 100,
                'type' => 'options',
                'options' => $statusValue,
                'renderer' => 'ancora_news/adminhtml_renderer_status'
            )
        );

        return parent::_prepareColumns();
    }
}