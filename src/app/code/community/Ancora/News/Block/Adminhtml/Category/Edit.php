<?php
/**
 * Ancora_News__Block_Adminhtml_Category_Edit
 *
 * Ancora News Adminhtml Category Edit
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @license GNU
 *
 */
class Ancora_News_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'category_id';
        $this->_blockGroup = 'ancora_news';
        $this->_controller = 'adminhtml_category';
        $this->_addButton(
            'saveandcontinue',
            array(
                'label' => $this->__('Save and Continue'),
                'onclick' => 'SaveAndContinueEdit()',
                'class' => 'save',
            ),
            100

        );
        $this->_formScripts[] = "
        function saveAndContinueEdit(){
            editForm.submit($('edit_form').action + 'back/edit/');
        }
        ";
        parent::__construct();
    }
}