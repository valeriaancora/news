<?php
/**
 * Ancora_News_Block_Adminhtml_Category
 *
 * Ancora News Block Adminhtml Category
 * @author  valeria ancora <valeria.ancora@thinkopen.it>
 * @version 0.2.0
 * @package  CMS
 * @liceAncoranse GNU
 *
 */
class Ancora_News_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'ancora_news';
        $this->_controller = 'adminhtml_category';
        $this->_headerText = $this->__('News Categories Management');
        parent::__construct();
    }
}